// ============================================
//
//  WDHT.cpp
//  Wireless Temperature and Humidity 
//    Sensor for McDougall Acres.
// 
//  Initialized by Chad A. Woitas @satiowadahc
//    June 2020
//
// ============================================

#include <Arduino.h>    // Needed for Basic IO // TODO: Remove later
#include <DHT.h>        // Needed for DHT Sensor
#include <ESP8266WiFi.h>     // Connect our device to WWW


#include <WDHT.h>       // Our Source File
#include <WDHTSecure.h> // Passwords




// ==================Sensors====================
// DHT Sensor
DHT dhtSens = DHT(DHTPIN, DHT22); // Main Temperature Sensor
uint32_t dhtLast = 0;             // Time Last Read DHT
uint32_t dhtDelay = 2000;         // Repeat Calls to DHT Sensor (0.5Hz from Data Sheet)
uint32_t dhtPrint = 2000;         // Repeat updates to serial print

// ============== Loop Variables ===============
double temperature = EPS;
double humidity = EPS;


// ============== Data Logging   ===============
typedef struct{
  uint32_t TR_idx; // Record Counted
  uint32_t TR_time;// Recorded time
  double TR_temp;  // Temperature at this packet
  double TR_hum;   // Humidity at this packet  
} TimeRecord;

#define HISTORY_LEN 100
TimeRecord Log_V[HISTORY_LEN] = {};
uint8_t Log_Traversal = 0;

uint16_t log_skipped = 0;
uint32_t log_count = 0;

#define loggingInterval 600000   // 10 minute interval packets
#define MS_SEC 1000u
#define SEC_MIN 60u
uint32_t loggingPrevious = 0;

// ========== Timing Variables  ================
uint32_t loopDelay = 500; // Time to repeat loop
uint32_t lastLoop  = 0;   // Time last loop ran

// Communication

WiFiServer server(80);
char * histo = new char[50];


// ================== Main ====================
void setup() {
  pinMode(LEDPIN, OUTPUT);
  digitalWrite(LEDPIN, HIGH);

  // System Communication
  #if defined(STARTUP_DEBUG) || defined(GEN_DEBUG)
    Serial.begin(BAUDRATE);
    while(!Serial);
    blinkLED();
  #endif

  WiFi.begin(LOCAL_SSID, LOCAL_PASS);
  while(WiFi.status() != WL_CONNECTED){
    #ifdef STARTUP_DEBUG
      Serial.print("Waiting for Wifi to connect... \n");
    #endif
    delay(100);
    blinkLED();
  }
  blinkLED();
  #ifdef STARTUP_DEBUG
    Serial.print("Wifi Connected @"); Serial.print(WiFi.localIP()); Serial.print(" \n");
  #endif

  server.begin();
  #ifdef STARTUP_DEBUG
    Serial.println("Server started");
  #endif
  blinkLED();

  // Sensors
  dhtSens.begin();
  delay(100);
  blinkLED();

  // Logs
  for(int i = 0; i<HISTORY_LEN; i++){
    Log_V[i].TR_idx = 0;
    Log_V[i].TR_temp = 0.0;
    Log_V[i].TR_hum = 0.0;
    Log_V[i].TR_time = 0;
  }

  // Testing Documentation
  blinkLED();
  delay(250);
  blinkLED();
  delay(250);
}

void loop() {
  // Avoid Repeated calls to loop
  while(1){
    ESP.wdtFeed(); // Feed the puppers
    processInputs();
    #ifdef GEN_DEBUG
    // processPrints();
    #endif
    processLogging();
    proccessServer();
    delay(20);

  }
}

//  ===================End Main ===============

/*
* Blink the LED
*/
void blinkLED(){
  digitalWrite(LEDPIN, !digitalRead(LEDPIN));
}

/*
* Collect Data from Sensors and Analyze As needed
*/
void processInputs(){
  uint32_t now = millis();
  if(now - dhtLast > dhtDelay){
    dhtLast = now; // Reset the Time
    temperature = dhtSens.readTemperature();
    humidity = dhtSens.readHumidity();
  }
}

void processLogging(){
  uint32_t now = millis();
  if(now - loggingPrevious > loggingInterval){
    loggingPrevious = now;
    
    // Set the packet ID and increment
    Log_V[Log_Traversal].TR_idx = log_count;
    log_count++;

    // Save Time
    Log_V[Log_Traversal].TR_time = now;

    // Save the Actual Data
    Log_V[Log_Traversal].TR_temp = temperature;
    Log_V[Log_Traversal].TR_hum = humidity;
    
    // Increment Traversal
    Log_Traversal++;
    if(Log_Traversal >= HISTORY_LEN){
      Log_Traversal = 0;
    }
  }
}

/*
* Send Data to Temperature
*/
void processPrints(){
  uint32_t now = millis();
  if(now - dhtPrint > dhtDelay){
    dhtPrint = now;
    Serial.print("Current Temperature: "); 
    Serial.print(temperature);
    Serial.print("°C Humidity: ");
    Serial.print(humidity);
    Serial.print("\n");
  }
}

/*
* Process connection requests
*/
void proccessServer(){
  uint32_t now = millis();
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  if(client.available()){
    // TODO ugly and delay
    delay(100); // Wait for client to be ready
    String request = client.readStringUntil('\r');
    #ifdef GEN_DEBUG
      Serial.print("Request: ");
      Serial.print(request);
      Serial.print("\n");
    #endif
    client.flush();

    // Print The Page
    // NOT HTTPS***

    // Header
    // TODO HTTPS?
    client.println(F("HTTP/1.1 200 OK"));
    client.println(F("Content-Type: text/html"));
    client.println(F(""));
    client.println(F("<!DOCTYPE HTML>"));
    client.println(F("<html>"));

    // TODO: CSS?

    client.println(F("<H1> Mcdougalls Sensor ID: </H1>"));
    client.println(F(" System Time: "));
    client.println(now/MS_SEC);

    // Body
    client.println(F("<H1> Temperature </H1>"));
    client.println(F("<pre>"));
    for(int i = 0; i<HISTORY_LEN; i++){
      // TODO Magic number
      // 0-80 offset by 40 allows for +/- 40 on scale
      printHisto(Log_V[i].TR_temp+40.0, 80.0, 
                  "T - " + String((now - Log_V[i].TR_time) / (MS_SEC)) +
                  " Temperature: " + String(Log_V[i].TR_temp),
                  client);
      if(now - Log_V[i].TR_time < 2){
        client.println(F("\n -------------  \n"));
      }
    }
    client.println(F("</pre>"));
    client.println(F("<H1> Humidity </H1>"));
    client.println(F("<pre>"));
    for(int i = 0; i<HISTORY_LEN; i++){
      // TODO Magic number
      printHisto(Log_V[i].TR_hum, 100.0, 
                  "T - " + String((now - Log_V[i].TR_time) / (MS_SEC)) +
                  " Humidity: " + String(Log_V[i].TR_hum),
                  client);
    }
    client.println(F("</pre>"));
    // Footer
    client.println(F("</html>"));
    delay(1);
    #ifdef GEN_DEBUG
      Serial.println(F("Client disconnected"));
      Serial.println(F(""));
    #endif
  }
} // End Server Process


/**
 * Send a histogram-type plot over serial for debugging and troubleshooting purposes.
 * @param value:    Value to plot
 * @param range:    Range of values to plot
 * @param message:  String to print following the plot
 */
void printHisto(double value, double range, String message, WiFiClient client){
  // Clear the histogram buffer, insert divisions every 10 characters
  for(int i=0;i<50;i++){
    if(i%10 == 0){ histo[i] = '|'; }
    else{ histo[i] = ' '; }
  }
  // Place the marker for the current value
  int idx = min(int((value/range)*50), 50);
  histo[idx] = 'o';

  // Print the histogram
  client.print(histo);
  client.print(F(" // "));
  client.print(message);
  client.print(F("<br>"));
} // End of printHisto(double, double, String)
