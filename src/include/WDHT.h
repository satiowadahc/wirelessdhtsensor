//============================================
//
//  WDHT.h 
//  Wireless Temperature and Humidity 
//    Sensor for McDougall Acres.
// 
//  Initialized by Chad A. Woitas @satiowadahc
//    June 2020
//
// ============================================

#ifndef _WDHT_H_
#define _WDHT_H_

// ==================== FLAGS ==================
// #define GEN_DEBUG      // Uncomment for generic debugging
// #define STARTUP_DEBUG  // Uncomment for initial start up info

// ==================== PINS ===================

#ifdef ESP8266

    #define LEDPIN 0
    #define DHTPIN 14 

#else
// Compilation will fail
#endif
// =================== System ==================

// Serial Setup
#define BAUDRATE 115200


// ==================== Utility ================
#define EPS 0.00001


// ======= Function Declarations ===============
void 
    blinkLED(),
    processInputs(),
    processLogging(),
    processPrints(),
    proccessServer(),
    printHisto(double, double, String, WiFiClient);

#endif





