// ============================================
//
//  WDHTSecure.h
//  Wireless Temperature and Humidity 
//    Sensor for McDougall Acres.
// 
//  Initialized by Chad A. Woitas @satiowadahc
//    June 2020
//
// ============================================

// DO NOT COMMIT CHANGES TO THIS TO GIT. Secured File.

#ifndef _WDHT_SECURED_
#define _WDHT_SECURED_


const char* LOCAL_SSID = ""; // Defined in Secure
const char* LOCAL_PASS = ""; // Defined in Secure

#define IO_USERNAME  ""     // Defined in Secure
#define IO_KEY       "" // Defined in Secure

#endif